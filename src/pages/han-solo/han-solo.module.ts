import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HanSoloPage } from './han-solo';

@NgModule({
  declarations: [
    HanSoloPage,
  ],
  imports: [
    IonicPageModule.forChild(HanSoloPage),
  ],
})
export class HanSoloPageModule {}
